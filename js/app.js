var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function($routes) {

  $routes.when('/',{
    templateUrl : 'views/home.html'
    // controller : 'HomeController'
  });

  $routes.when('/exterior',{
    templateUrl : 'views/exterior.html'
    // controller : 'ExteriorController'
  });

  $routes.when('/interior',{
    templateUrl : 'views/interior.html'
    //controller : 'InteriorController'
  });

  $routes.when('/gallery',{
    templateUrl : 'views/gallery.html',
    // controller : 'GalleryController'
  });

  $routes.when('/form',{
    templateUrl : 'views/form.html'
    // controller : 'FormController'
  });

  $routes.when('/live-preview',{
    templateUrl : 'views/live-preview.html'
  });

  $routes.otherwise({
    redirectTo : '/'
  });

}]);

app.directive('headerSection', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/header-section.html'
	};
});
app.directive('footerNav', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/footer-nav.html'
	};
});
app.directive('formSection', function(){
  return {
    restrict: 'E',
    templateUrl: 'views/form-section.html'
  };
});

// app.controller('HomeController', function($scope) {

// });

// app.controller('ExteriorController', function($scope) {

// });

app.controller('InteriorController', function() {
  
      if (  ! Detector.webgl ) {
        Detector.addGetWebGLMessage();
      }
      else
      {
        var container, camera, scene, renderer, controls, geometry, mesh, backgroundButton;

        var animate = function(){

          window.requestAnimationFrame( animate );

          controls.update();
          renderer.render(scene, camera);

        };

        container = document.getElementById( 'container3d' );

        camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 1, 1100);

        controls = new THREE.DeviceOrientationControls( camera );

        scene = new THREE.Scene();

        var geometry = new THREE.SphereGeometry( 500, 316, 18 );
        geometry.applyMatrix( new THREE.Matrix4().makeScale( -1, 1, 1 ) );

      
        var material = new THREE.MeshBasicMaterial( {
          map: THREE.ImageUtils.loadTexture( 'img/image.jpg' )
        } );

        var mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        var geometry = new THREE.BoxGeometry( 100, 100, 100, 4, 4, 4 );
        var mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.domElement.style.position = 'relative';
        renderer.domElement.style.top = 0;
        container.appendChild(renderer.domElement);

         // TODO: change this to angular equivalent
        window.addEventListener('resize', function() {
          camera.aspect = window.innerWidth / window.innerHeight;
          camera.updateProjectionMatrix();
          renderer.setSize( window.innerWidth, window.innerHeight );

        }, false);

        animate();

        console.log('INTERIOR BEGIN');
      }            

});

// TODO MARIUS - maybe split these controllers out into their own .js filea?
app.controller('GalleryController', function() { 

  var gallery = $('#gallery'),
  gallery1 = $('#gallery1');

  console.log("init gallery");
  
  $('#gallery').photobox('a', {
      thumbs: true,
      loop: false
  }, callback);

  $('#gallery1').photobox('a', {
      thumbs: true,
      loop: false
  }, callback);

  function callback(){
      console.log('image has been loaded');
  }

   console.log("GALLERY CONTROLLER BEGIN");

});

// app.controller('FormController', function($scope) {

// });